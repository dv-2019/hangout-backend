const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/user");

exports.login = (req, res, next) => {
    const { username, password } = req.body;
    User.findOne({ where: { username } })
        .then(result => {
            if (!result) {
                res.sendStatus(401);
                return;
            }

            const dbUser = result.toJSON();
            bcrypt.compare(password, dbUser.password, (err, same) => {
                if (same) {
                    res.status(200).json({
                        token: jwt.sign(
                            {
                                id: dbUser.id,
                            },
                            process.env.JWT_SECRET_KEY,
                            {
                                expiresIn: "8h",
                            }
                        ),
                    });
                } else {
                    res.sendStatus(401);
                    return;
                }
            });
        })
        .catch(err => {
            next(err);
        });
}

exports.createUser = (req, res, next) => {
    let hash = bcrypt.hashSync(req.body.password, 10);
    req.body.password = hash;
    User.create(req.body)
        .then(result => {
            const dbUser = result.toJSON();
            res.status(200).json({
                token: jwt.sign(
                    {
                        id: dbUser.id,
                    },
                    process.env.JWT_SECRET_KEY,
                    {
                        expiresIn: "8h",
                    }
                ),
            });
        })
        .catch(error => {
            console.log(error)
            next(error);
        });
}

exports.checkExpired = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        jwt.verify(token, process.env.JWT_SECRET_KEY);
        res.status(200).json({
            isExpired: false,
        });
        return;
    } catch (error) {
        res.status(200).json({
            isExpired: true,
        });
        return;
    }
}