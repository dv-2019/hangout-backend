const multer = require("multer");
const aws = require("aws-sdk");
const multerS3 = require("multer-s3");

aws.config.update({
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
});

const s3 = new aws.S3();

const storage = multerS3({
    s3,
    bucket: process.env.AWS_S3_BUCKET_NAME,
    key: function (req, file, cb) {
        cb(
            null,
            process.env.AWS_S3_FOLDER_NAME +
            "/" +
            Date.now().toString()
            +
            file.originalname.substr(0, 8)
        );
    },
    contentType: multerS3.AUTO_CONTENT_TYPE
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype === "image/jpeg" || file.mimetype === "image/png") {
        cb(null, true);
    } else {
        cb(null, false)
    }
}

const upload = multer(
    {
        storage,
        limits: {
            fileSize: 1024 * 1024 * 5
        },
        fileFilter: fileFilter
    }
);

exports.uploadOneImage = upload.single("image");

exports.uploadManyImage = upload.array("images");

exports.deleteOneImage = (params) => {
    return s3.deleteObject(params).promise();
}

exports.deleteManyImages = (params) => {
    return s3.deleteObjects(params).promise();
}