const User = require("../models/user");
const UserProfileImage = require("../models/userProfileImage");

exports.checkAvailability = (req, res, next) => {
    let username = req.query.username;
    User.findOne({ where: { username } })
        .then(result => {
            if (result) {
                res.status(200).json({
                    isAvailable: false,
                });
            } else {
                res.status(200).json({
                    isAvailable: true,
                });
            }
        })
        .catch(error => {
            next(error);
        })
}

exports.getUserProfile = (req, res, next) => {
    User.findByPk(req.userData.id, {
        attributes: ["id", "fullName", "username","dateOfBirth"],
        include: [
            {
                model: UserProfileImage,
                attributes: ["url"]
            }
        ]
    })
        .then(result => {
            const dbUser = result.toJSON();
            res.status(200).json(dbUser);
        })
        .catch(error => {
            console.log(error)
            next(error);
        })
}

exports.updateProfile = (req, res, next) => {
    User.update(req.body, { where: { id: req.userData.id } })
        .then(result => {
            res.status(200).json({
                updated: result[0]
            })
        })
        .catch(err => next(err))
}

exports.updateProfileImage = async (req, res, next) => {
    console.log(req.userData.id)
    const { file } = req;
    const { id } = req.userData;
    try {
        let oldImage = await UserProfileImage.findOne({ where: { userId: id } });
        if (oldImage) {
            let { key } = oldImage.toJSON();
            deleteOneImage({
                Bucket: process.env.AWS_S3_BUCKET_NAME,
                Key: key,
            })
            oldImage.destroy();
        }

        let result = await UserProfileImage.create({
            userId: id,
            url: file.location,
            key: file.key,
        });
        res.status(200).json(result.toJSON());
    } catch (error) {
        console.log(error)
        next(error)
    }
}