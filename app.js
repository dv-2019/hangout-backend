require("dotenv").config();
const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");
const morgan = require("morgan");
require("./database");
require("./models/_relations");

const authRoute = require("./routes/auth");
const userRoute = require("./routes/user");
// const realEstateRoute = require("./routes/realEstate");

//Parsing JSON body
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Use logging
app.use(morgan("dev"));

//Handle CORS
app.use(cors());

//Set up main routes
app.use("/auth", authRoute);
app.use("/user", userRoute);
// app.use("/real-estate", realEstateRoute);

//Handle error 
app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
});
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

//Close database connection when program is terminated
process.on("exit", () => {
    global.db.close();
});

//Start server
const port = process.env.PORT;
app.listen(port);