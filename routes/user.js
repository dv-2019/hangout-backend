const express = require("express");
const router = express.Router();
const {
    checkAvailability,
    getUserProfile,
    updateProfile,
    updateProfileImage,
} = require("../controllers/user");
const checkAuth = require("../middleware");
const { uploadOneImage } = require("../controllers/file");

router.get("/check-availability", checkAvailability);

router.get("/profile", checkAuth, getUserProfile);

router.patch("/", checkAuth, updateProfile);

router.put("/image", checkAuth, uploadOneImage, updateProfileImage);

module.exports = router;
