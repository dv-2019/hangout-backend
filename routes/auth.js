const express = require("express");
const router = express.Router();
const { login, createUser, checkExpired } = require("../controllers/auth");

router.post("/login", login);

router.post("/signup", createUser);

router.get("/check-expired", checkExpired);

module.exports = router;