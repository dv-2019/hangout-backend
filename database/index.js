const Sequelize = require("sequelize");

class Database {
    constructor() {
        this._connect();
    }
    _connect() {
        const sequelize = new Sequelize(
            process.env.DB_NAME_ENV,
            process.env.DB_USER_ENV,
            process.env.DB_PASS_ENV,
            {
                host: process.env.DB_HOST_ENV,
                dialect: process.env.DB_DIALECT_ENV,
            }
        );
        sequelize.sync();
        global.db = sequelize;
        sequelize
            .authenticate()
            .then(() => {
                console.log("Connection has been established successfully.");
            })
            .catch(err => {
                console.error("Unable to connect to the database:", err);
                throw err
            });
    }
}

module.exports = new Database();