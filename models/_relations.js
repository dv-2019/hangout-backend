const User = require("./user");
const UserProfileImage = require("./userProfileImage");

User.hasOne(UserProfileImage, {
    foreignKey: {
        allowNull: false,
    },
    onDelete: "CASCADE",
});