const Sequelize = require("sequelize");
const Model = Sequelize.Model;

class UserProfileImage extends Model { }
UserProfileImage.init(
    {
        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
        },
        url: {
            type: Sequelize.STRING,
            allowNull: false,
        },
        key: {
            type: Sequelize.STRING,
            allowNull: false,
        },
    },
    {
        sequelize: global.db,
        modelName: "userProfileImage",
        charset: "utf8",
        collate: "utf8_general_ci",
        underscored: true,
    },
);

module.exports = UserProfileImage;