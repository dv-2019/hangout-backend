const Sequelize = require("sequelize");
const Model = Sequelize.Model;

class User extends Model { }
User.init(
    {
        id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true,
        },
        fullName: {
            type: Sequelize.STRING(63),
            allowNull: false,
        },
        username: {
            type: Sequelize.STRING(31),
            unique: true,
            allowNull: false,
        },
        password: {
            type: Sequelize.STRING(60, true),
            allowNull: false,
        }
        ,
        dateOfBirth: {
            type: Sequelize.STRING,
            allowNull: false
        }
    },
    {
        sequelize: global.db,
        modelName: "user",
        charset: "utf8",
        collate: "utf8_general_ci",
        underscored: true,
    },
);

module.exports = User;